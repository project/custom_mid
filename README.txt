CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Use case
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 
INTRODUCTION
------------

This module allows site builders to create media with their custom mid.
If the media with given mid is already exist a message will occur that
"Mid already exists".
 
USE CASE
------------

1. In case when you write any business logic on the basis of media id 
   you can use this module.
2. When you transfer data from one instance to another you can set id 
   according your choice.

REQUIREMENTS
------------

There is no requirement.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/895232/ for further information.

CONFIGURATION
-------------

There is no configuration.

MAINTAINERS
-----------

Current maintainers:
* Stangl David (Duwid) - https://www.drupal.org/u/duwid