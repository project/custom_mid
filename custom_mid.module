<?php

/**
 * @file
 * Implements hook_form_alter().
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\MediaForm;

/**
 * Hook_form_alter()
 */
function custom_mid_form_alter(&$form, FormStateInterface $form_state, $form_id) {

  $form_object = $form_state->getFormObject();
  if (($form_object instanceof MediaForm) && \Drupal::currentUser()
    ->hasPermission('custom_mid access')) {
    $media_object = $form_object->getEntity();
    if (!isset($media_object->mid->value)) {
      $form['custom_mid_field'] = [
        '#type' => 'textfield',
        '#title' => t('Media id'),
        '#default_value' => '',
        '#size' => 15,
        '#maxlength' => 15,
        '#weight' => -50,
        '#element_validate' => ['attach_custom_mid_field_to_media'],
      ];
      $form['#validate'][] = 'custom_mid_form_validate';
    }
  }
}

/**
 * Form validate.
 */
function custom_mid_form_validate(&$form, FormStateInterface $form_state) {
  $custom_mid = $form_state->getValue('custom_mid_field');
  if ('' != $custom_mid) {
    if (!is_numeric($custom_mid)) {
      $form_state->setErrorByName('custom_mid_field', t("Media id is not numeric."));
    }
    $connection = \Drupal::database();
    $result = $connection->select('media', 'n')
      ->fields('n')
      ->condition('mid', $custom_mid, '=')
      ->execute()
      ->fetchAssoc();
    if ($result) {
      $form_state->setErrorByName('custom_mid_field', t("Media id already exists."));
    }
  }
}

/**
 * Callback function.
 */
function attach_custom_mid_field_to_media($element, FormStateInterface $form_state, $form) {
  $media = $form_state->getFormObject()->getEntity();
  $media->custom_mid_field = $form_state->getValue('custom_mid_field');
}

/**
 * Implements hook_entity_presave().
 */
function custom_mid_entity_presave(EntityInterface $entity) {
  if ($entity->isNew() && $entity->getEntityTypeId() == 'media') {
    if ('' != $entity->custom_mid_field && is_numeric($entity->custom_mid_field)) {
      $entity->set('mid', $entity->custom_mid_field);
    }
  }
}
